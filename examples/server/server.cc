#include <QDebug>
#include <iostream>
#include "nzmqt.hpp"
#include "brokerclient.h"
#ifdef WIN32
# include <Shlwapi.h>
# include <windows.h>
# include <process.h>
#else
# include <unistd.h>
#endif

#ifdef WIN32
# pragma comment(lib, "Shlwapi.lib")
#endif


// ░░█▀░░░░░░░░░░░▀▀███████░░░░
// ░░█▌░░░░░░░░░░░░░░░▀██████░░░
// ░█▌░░░░░░░░░░░░░░░░███████▌░░
// ░█░░░░░░░░░░░░░░░░░████████░░
// ▐▌░░░░░░░░░░░░░░░░░▀██████▌░░
// ░▌▄███▌░░░░▀████▄░░░░▀████▌░░
// ▐▀▀▄█▄░▌░░░▄██▄▄▄▀░░░░████▄▄░
// ▐░▀░░═▐░░░░░░══░░▀░░░░▐▀░▄▀▌▌
// ▐░░░░░▌░░░░░░░░░░░░░░░▀░▀░░▌▌
// ▐░░░▄▀░░░▀░▌░░░░░░░░░░░░▌█░▌▌
// ░▌░░▀▀▄▄▀▀▄▌▌░░░░░░░░░░▐░▀▐▐░
// ░▌░░▌░▄▄▄▄░░░▌░░░░░░░░▐░░▀▐░░
// ░█░▐▄██████▄░▐░░░░░░░░█▀▄▄▀░░
// ░▐░▌▌░░░░░░▀▀▄▐░░░░░░█▌░░░░░░
// ░░█░░▄▀▀▀▀▄░▄═╝▄░░░▄▀░▌░░░░░░
// ░░░▌▐░░░░░░▌░▀▀░░▄▀░░▐░░░░░░░
// ░░░▀▄░░░░░░░░░▄▀▀░░░░█░░░░░░░
// ░░░▄█▄▄▄▄▄▄▄▀▀░░░░░░░▌▌░░░░░░
// ░░▄▀▌▀▌░░░░░░░░░░░░░▄▀▀▄░░░░░
// ▄▀░░▌░▀▄░░░░░░░░░░▄▀░░▌░▀▄░░░
// ░░░░▌█▄▄▀▄░░░░░░▄▀░░░░▌░░░▌▄▄
// ░░░▄▐██████▄▄░▄▀░░▄▄▄▄▌░░░░▄░
// ░░▄▌████████▄▄▄███████▌░░░░░▄
// ░▄▀░██████████████████▌▀▄░░░░
// ▀░░░█████▀▀░░░▀███████░░░▀▄░░
// ░░░░▐█▀░░░▐░░░░░▀████▌░░░░▀▄░
// ░░░░░░▌░░░▐░░░░▐░░▀▀█░░░░░░░▀
// ░░░░░░▐░░░░▌░░░▐░░░░░▌░░░░░░░
// ░╔╗║░╔═╗░═╦═░░░░░╔╗░░╔═╗░╦═╗░
// ░║║║░║░║░░║░░░░░░╠╩╗░╠═╣░║░║░
// ░║╚╝░╚═╝░░║░░░░░░╚═╝░║░║░╩═╝░

static void
getClientInfo(broker::ClientInfo *aInfo)
{
  static broker::ClientInfo info{};

  bool const initialized{info.IsInitialized()};

  std::cerr << Q_FUNC_INFO << " aInfo:\n" << aInfo->Utf8DebugString() << std::endl;
  std::cerr << Q_FUNC_INFO << "  info:\n" << info.Utf8DebugString() << std::endl;

  if (initialized) {
    aInfo->CopyFrom(info);
    std::cerr << Q_FUNC_INFO << "  info:\n" << info.Utf8DebugString() << std::endl;
    std::cerr << Q_FUNC_INFO << " aInfo:\n" << aInfo->Utf8DebugString() << std::endl;
    return;
  }

#if defined(WIN32) && !defined(HOST_NAME_MAX)
# define HOST_NAME_MAX 1024
#endif
#ifndef MAX_PATH
# define MAX_PATH 4098
#endif
#if defined(WIN32) && !defined(getpid)
# define getpid _getpid
#endif
  char hostname[HOST_NAME_MAX]{};
  char name[MAX_PATH]{};

  int ret{gethostname(hostname, HOST_NAME_MAX)};

#ifdef WIN32
  if (ret == -1) {
    WSADATA wsaData{};
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    gethostname(hostname, HOST_NAME_MAX);
    WSACleanup();
  }
#endif

#ifdef WIN32
  GetModuleFileNameA(0, name, MAX_PATH);
//  PathStripPathA(name);
#else
  ret = static_cast<int>(readlink("/proc/self/exe", name, MAX_PATH));
#endif

  info.set_hostname(hostname);
  info.set_name(name);
  info.set_pid(getpid());

  aInfo->CopyFrom(info);

  std::cerr << Q_FUNC_INFO << "  info:\n" << info.Utf8DebugString() << std::endl;
  std::cerr << Q_FUNC_INFO << " aInfo:\n" << aInfo->Utf8DebugString() << std::endl;
}

#if 0
Broker::Broker(QObject *parent)
  : QObject(parent)
  , m_zmqContext{nzmqt::createDefaultContext(this)}
  , m_socket{m_zmqContext->createSocket(nzmqt::ZMQSocket::TYP_REQ, this)}
  , m_topics{}
{
  m_zmqContext->setObjectName("ZMQ_CONTEXT");
  m_zmqContext->start();

  m_socket->setObjectName("ZMQ_SOCKET_CLIENT");
  m_socket->connectTo("tcp://127.0.0.1:1234");

  connect(m_socket, SIGNAL(messageReceived(QList<QByteArray>)), SLOT(messageReceived(QList<QByteArray>)));
}

Broker::~Broker()
{
  m_socket->setLinger(0);
}

void
Broker::send_message()
{
  using broker::PublishRequest;
  using broker::PublishResponse;

  PublishRequest request{};
  PublishResponse response{};

  getClientInfo(request.mutable_client());

  for (auto const &topic : m_topics) {
    request.add_topics(topic);
  }

  qDebug() << "REQUEST[" << request.Utf8DebugString().c_str() << "]";

  QList<QByteArray> msg{};
  std::string msgString{};

  broker::RequestType type{};
  type.set_type(broker::RequestType::PUBLISH);
  type.SerializeToString(&msgString);
  msg += QByteArray::fromStdString(msgString);

  request.SerializeToString(&msgString);
  msg += QByteArray::fromStdString(msgString);
  m_socket->sendMessage(msg);
}

void
Broker::messageReceived(QList<QByteArray> const& message)
{
  qDebug() << "[CLIENT]" << sender()->objectName();
  qDebug() << "message.count():" << message.count();
  qDebug() << "message:" << message;
}
#endif

namespace broker {

Publisher::Publisher()
  : m_context{new zmq::context_t}
  , m_socket{new zmq::socket_t(*m_context, ZMQ_REQ)}
  , m_broker{}
  , m_topics{}
{
  std::cerr << Q_FUNC_INFO << ">" << std::endl;
  std::cerr << Q_FUNC_INFO << "= " << "ctx: 0x" << m_context << " sock: 0x" << m_socket << std::endl;
  std::cerr << Q_FUNC_INFO << "<" << std::endl;
}

Publisher::~Publisher()
{
  std::cerr << Q_FUNC_INFO << ">" << std::endl;
  delete m_socket;
  delete m_context;
  std::cerr << Q_FUNC_INFO << "<" << std::endl;
}

void
Publisher::connectTo(std::string const &a_broker)
{
  std::cerr << Q_FUNC_INFO << ">" << std::endl;
  m_broker = a_broker;
  m_socket->connect(a_broker);
  std::cerr << Q_FUNC_INFO << "<" << std::endl;
}

void
Publisher::addTopic(std::string const &a_topic)
{
  std::cerr << Q_FUNC_INFO << ">" << std::endl;
  if (std::find(std::begin(m_topics), std::end(m_topics), a_topic) == std::end(m_topics)) {
    m_topics.push_back(a_topic);
    std::cerr << Q_FUNC_INFO << "= " << "topic " << a_topic << " added" << std::endl;
  } else {
    std::cerr << Q_FUNC_INFO << "= " << "topic " << a_topic << " already exist" << std::endl;
  }
  std::cerr << Q_FUNC_INFO << "<" << std::endl;
}

#if 0

zmq::message_t message(string.size());
memcpy (message.data(), string.data(), string.size());
#endif

void
Publisher::publish()
{
  std::cerr << Q_FUNC_INFO << ">" << std::endl;

  if (m_topics.empty()) {
    std::cerr << Q_FUNC_INFO << "= " << "There are no topics..." << std::endl;
    std::cerr << Q_FUNC_INFO << "<" << std::endl;
    return;
  }

  static std::string msg{};

  RequestType requestType{};
  requestType.set_type(RequestType::PUBLISH);
  requestType.SerializeToString(&msg);

//  PublishRequest publishRequest{request.mutable_publish()};
  PublishRequest publishRequest{};
  getClientInfo(publishRequest.mutable_client());
  std::cerr << Q_FUNC_INFO << "= dump: " << publishRequest.Utf8DebugString() << std::endl;

  for (auto &topic : m_topics)
    publishRequest.add_topics(topic);

  std::cerr << Q_FUNC_INFO << "= " << "requestType bytesize: " << requestType.ByteSize() << " publishRequest bytesize: " << publishRequest.ByteSize() << std::endl;

  std::cerr << Q_FUNC_INFO << "= " << "Making a request" << std::endl;

  std::cerr << Q_FUNC_INFO << "= " << "Sending requestType..." << std::endl;
  requestType.SerializeToString(&msg);
  send(msg, ZMQ_SNDMORE);

  std::cerr << Q_FUNC_INFO << "= " << "Sending publishRequest..." << std::endl;
  publishRequest.SerializeToString(&msg);
  send(msg);

  std::cerr << Q_FUNC_INFO << "= " << "Waiting for reply..." << std::endl;
  std::string reply{receive()};

  std::cerr << Q_FUNC_INFO << "= " << "Got reply, parsing..." << std::endl;
  PublishResponse response{};
  response.ParseFromString(reply);
  int const requestResult{response.result()};
  switch (requestResult) {
    case PublishResponse::OK:
      std::cerr << Q_FUNC_INFO << "= " << "Request result: OK" << std::endl;
      break;
    case PublishResponse::NOT_INITIALIZED:
      std::cerr << Q_FUNC_INFO << "= " << "Request result: NOT_INITIALIZED" << std::endl;
      break;
    case PublishResponse::EMPTY:
      std::cerr << Q_FUNC_INFO << "= " << "Request result: EMPTY" << std::endl;
      break;
    case PublishResponse::PARTIAL:
      std::cerr << Q_FUNC_INFO << "= " << "Request result: PARTIAL" << std::endl;
      break;
    default:
      break;
  }
  std::cerr << Q_FUNC_INFO << "= " << "Topic results: " << response.topic_result_size() << std::endl;

  std::cerr << Q_FUNC_INFO << "<" << std::endl;
}

void
Publisher::unpublish()
{
  std::cerr << Q_FUNC_INFO << ">" << std::endl;

  static std::string msg{};

  RequestType requestType{};
  requestType.set_type(RequestType::UNPUBLISH);
  requestType.SerializeToString(&msg);
  send(msg, ZMQ_SNDMORE);

  UnpublishRequest unpublishRequest{};
  getClientInfo(unpublishRequest.mutable_client());
  unpublishRequest.SerializeToString(&msg);
  std::cerr << Q_FUNC_INFO << "= " << unpublishRequest.Utf8DebugString() << std::endl;
  send(msg);

  receive();

  std::cerr << Q_FUNC_INFO << "<" << std::endl;
}

void
Publisher::send(std::string a_message, int a_flag)
{
  std::cerr << Q_FUNC_INFO << ">" << std::endl;
  zmq::message_t message(a_message.size());
  memcpy(message.data(), a_message.data(), a_message.size());
  std::cerr << Q_FUNC_INFO << "= " << "sending message (" << a_message.size() << " bytes), flag: " << a_flag << std::endl;
  m_socket->send(message, a_flag);
  std::cerr << Q_FUNC_INFO << "= " << "message sent" << std::endl;
  std::cerr << Q_FUNC_INFO << "<" << std::endl;
}

std::string
Publisher::receive()
{
  std::cerr << Q_FUNC_INFO << ">" << std::endl;
  zmq::message_t message{};
  m_socket->recv(&message);
  std::string msg(static_cast<char*>(message.data()), message.size());
  std::cerr << Q_FUNC_INFO << "<" << std::endl;
  return msg;
}


} // namespace broker
