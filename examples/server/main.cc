// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <signal.h>

#include <thread>
#include <chrono>
#include <random>
#include <iomanip>

#include <broker/server.h>
#include <broker/broker_server.pb.h>

#include "topicdata.pb.h"

TopicData values[3];

std::random_device r{};
std::mt19937 gen(r());
std::uniform_real_distribution<float> target(0.0f, 120.0f);
std::uniform_real_distribution<float> speed(5.f, 35.f);

std::vector<broker::string::hash_t> hashes{};
size_t hashesSize{};

enum Flags {
  Topic1 = 1 << 0,
  Topic2 = 1 << 1,
  Topic3 = 1 << 2
};

int32_t flags{};

void server_init(int32_t argc, char const **argv)
{
  std::string const ip{argv[1]};
  uint32_t const pubPort{static_cast<uint32_t>(std::atoi(argv[2]))};
  uint32_t const reqPort{static_cast<uint32_t>(std::atoi(argv[3]))};

  auto &server = broker::server();
  server.connect("tcp://10.0.0.2:1234");
  char buff[256]{};
  snprintf(buff, 256, "tcp://%s", ip.c_str());
  std::cout << "buff: " << buff << std::endl;
  server.bind(buff, pubPort, reqPort);

  broker::topics_t topics{};

  for (int32_t i = 4; i < argc; ++i) {
    topics.push_back(argv[i]);
    broker::string::hash_t hash{broker::string::hash(argv[i])};
    hashes.push_back(hash);
    switch (hash) {
      case broker::string::hash("Topic1"): flags |= Topic1; break;
      case broker::string::hash("Topic2"): flags |= Topic2; break;
      case broker::string::hash("Topic3"): flags |= Topic3; break;
    }
  }
  hashesSize = hashes.size();
  server.registerTopics(topics);

  values[0].set_target(target(gen));
  values[0].set_speed(speed(gen));
  values[0].set_value(0.f);
  values[1].set_target(target(gen));
  values[1].set_speed(speed(gen));
  values[1].set_value(0.f);
  values[2].set_target(target(gen));
  values[2].set_speed(speed(gen));
  values[2].set_value(0.f);
}

void server_update(float const &dt)
{
#define UPDATE_VALUES(i) \
  float const delta{ values[i].speed() * (dt / 1000.f) }; \
  if (values[i].value() < values[i].target()) { \
    values[i].set_value(values[i].value() + delta); \
    if (values[i].value() >= values[i].target()) { \
      values[i].set_target(target(gen)); \
      values[i].set_speed(speed(gen)); \
    } \
  } else { \
    values[i].set_value(values[i].value() - delta); \
    if (values[i].value() <= values[i].target()) { \
      values[i].set_target(target(gen)); \
      values[i].set_speed(speed(gen)); \
    } \
  }

  if (flags & Topic1) {
    UPDATE_VALUES(0)
  }
  if (flags & Topic2) {
    UPDATE_VALUES(1)
  }
  if (flags & Topic3) {
    UPDATE_VALUES(2)
  }
}

void server_handle_requests()
{
  auto &server = broker::server();

  TopicData data{};

  broker::messages_t incoming{server.getIncoming()};
  if (!incoming.empty() && !incoming[0].empty()) {
    std::cout << "\nrequest!" << incoming.size() << incoming[0].size() << std::endl;
    broker::Hash requestHash{};
    requestHash.ParseFromString(incoming[0]);

    broker::messages_t replies{};

    if (requestHash.hash() == broker::string::hash("DataRequest")) {
//        std::cout << "DataRequest!" << std::endl;

      broker::DataRequest request{};
      request.ParseFromString(incoming[1]);

      broker::message_t message{};

      auto const &topic = request.topic();
      broker::string::hash_t const topicHash{broker::string::hash(topic.c_str())};

//        std::cerr << "Writing data for " << topic << std::endl;
      switch (topicHash) {
        case broker::string::hash("Topic1"): data = values[0]; break;
        case broker::string::hash("Topic2"): data = values[1]; break;
        case broker::string::hash("Topic3"): data = values[2]; break;
      }

      SERIALIZE_HASH(topic.c_str(), message);
      replies.push_back(message);

      data.SerializeToString(&message);
      replies.push_back(message);
    }

    std::cerr << "Replies size: " << replies.size() << std::endl;
    server.sendReply(replies);
  }
}

void server_publish()
{
  auto &server = broker::server();

  TopicData const *data{};

  broker::messages_t messages{};
  broker::message_t message{};

  char const* name{};
  for (size_t i = 0; i < hashesSize; ++i) {
    switch (hashes[i]) {
    case broker::string::hash("Topic1"): name = "Topic1"; data = &values[0]; break;
    case broker::string::hash("Topic2"): name = "Topic2"; data = &values[1]; break;
    case broker::string::hash("Topic3"): name = "Topic3"; data = &values[2]; break;
    }

    SERIALIZE_HASH(name, message);
    messages.emplace_back(message);
    data->SerializeToString(&message);
    messages.emplace_back(message);
  }
  server.publish(messages);
}

int main(int32_t argc, char const **argv)
{
  broker::catch_em_all({SIGINT}, {SIGILL, SIGFPE, SIGSEGV, SIGTERM, SIGABRT});

//  std::string f = format_number(12578160);

  if (argc < 5) {
    std::cerr << "Usage: " << argv[0] << " <ip> <pubport> <reqport> topic1 (topic2).." << std::endl;
    return EXIT_FAILURE;
  }

  server_init(argc, argv);

  bool done{};

  using std::chrono::duration_cast;
  using std::chrono::duration;
  using clock = std::chrono::high_resolution_clock;

  auto lastTime = clock::now();

  uint64_t ups{}, lastUps{};
  float upsTimer{};

  auto &server = broker::server();

  while (!done) {
    auto const nowTime = clock::now();
    auto frameTime = nowTime - lastTime;
    float frameTimeLog{duration_cast<duration<float, std::milli>>(frameTime).count()};

    lastTime = nowTime;

    upsTimer += frameTimeLog;

    server.ping();

    if (upsTimer >= 1000.f) {
      upsTimer = 0.f;
      lastUps = ups;
      ups = 0;

      std::cerr << " dt: " << std::fixed << std::setw(9) << std::setprecision(5) << frameTimeLog << "ms";
      std::cerr << " ups: " << lastUps << " upm: " << lastUps * 60;
      if (flags & Topic1)
        std::cerr << " t1: {" << std::fixed << std::setw(7) << std::setprecision(3) << values[0].value() << ", " << std::fixed << std::setw(7) << std::setprecision(3) << values[0].target() << ", " << std::fixed << std::setw(7) << std::setprecision(3) << values[0].speed() << "} ";
      if (flags & Topic2)
        std::cerr << " t2: {" << std::fixed << std::setw(7) << std::setprecision(3) << values[1].value() << ", " << std::fixed << std::setw(7) << std::setprecision(3) << values[1].target() << ", " << std::fixed << std::setw(7) << std::setprecision(3) << values[1].speed() << "} ";
      if (flags & Topic3)
        std::cerr << " t3: {" << std::fixed << std::setw(7) << std::setprecision(3) << values[2].value() << ", " << std::fixed << std::setw(7) << std::setprecision(3) << values[2].target() << ", " << std::fixed << std::setw(7) << std::setprecision(3) << values[2].speed() << "} ";
      std::cerr << "\n";
    }

    server_handle_requests();

    server_update(frameTimeLog);
    ups++;

    server_publish();
  }

  server.close();

  return EXIT_SUCCESS;
}
