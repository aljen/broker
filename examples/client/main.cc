// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <QApplication>
#include <cstdio>
#include <cstdlib>
#include "window.h"

int main(int argc, char **argv)
{
  QApplication app(argc, argv);
  Window window{};
  window.show();
  return app.exec();
}

#if 0
#include <cstdio>
#include <cstdlib>
#include <iostream>

void handle_messages(broker::messages_t a_messages)
{
  size_t const size{a_messages.size()};
  std::cerr << "Messages size: " << size << std::endl;
  assert(size != 0 && (size % 2 == 0));

  broker::Hash replyHash{};
  broker::TopicData data{};
  for (size_t i = 0; i < size; i += 2) {
    replyHash.ParseFromString(a_messages[i]);
    data.ParseFromString(a_messages[i + 1]);

    std::string topic{};
    switch (replyHash.hash()) {
      case broker::hash_fnv1a("Topic1"): topic = "Topic1"; break;
      case broker::hash_fnv1a("Topic2"): topic = "Topic2"; break;
      case broker::hash_fnv1a("Topic3"): topic = "Topic3"; break;
      default: topic = "unknown"; break;
    }

    std::cerr << "message #" << i + 1<< "/" << size << std::endl;
    std::cerr << "message hash: 0x" << std::hex << replyHash.hash() << " (matches topic: " << topic << ")" << std::endl;
    std::cerr << "message #" << i + 2 << "/" << size << std::endl;
    std::cerr << "message data for topic: " << topic << std::endl;
    std::cerr << "Data:\n" << data.Utf8DebugString() << std::endl;
  }

}

int main(int32_t argc, char const **argv)
{
  (void)argc;
  (void)argv;

  broker::Client &client = broker::client();
  client.connect(fmt::format("tcp://{}:{}", broker::gethostname(), 1234));

  broker::topics_t const topics{"Topic1", "Topic3", "Topic2"};
  client.subscribe(topics);
  broker::messages_t replies{client.request(topics)};

  std::cerr << "Requested data:" << std::endl;
  handle_messages(replies);

  std::cerr << "Subscribed data:" << std::endl;
  for (;;)
    handle_messages(client.receive());

  return EXIT_SUCCESS;
}
#endif
