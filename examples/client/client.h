#ifndef BROKERCLIENT_H
#define BROKERCLIENT_H

#include <vector>
#include <string>
#include <broker_server.pb.h>

namespace zmq {
class context_t;
class socket_t;
}

namespace broker {

class Publisher;
static Publisher& publisher();

class Publisher {
 public:
   void connectTo(std::string const &a_broker);
   void addTopic(std::string const &a_topic);
   void publish();
   void unpublish();

 private:
  Publisher();
  ~Publisher();

  Publisher(Publisher const&) = delete;
  void operator=(Publisher const&) = delete;

  void send(std::string a_message, int a_flag = 0);
  std::string receive();

 private:
  zmq::context_t *m_context;
  zmq::socket_t *m_socket;
  std::string m_broker;
  std::vector<std::string> m_topics;

  friend Publisher& publisher();
};

static inline Publisher& publisher() {
  static Publisher s_publisher{};
  return s_publisher;
}

} // namespace broker

#if 0
class Broker : public QObject {
  Q_OBJECT

 public:
  explicit Broker(QObject *parent = nullptr);
  ~Broker();

  Broker(Broker const&) = delete;
  void operator=(Broker const&) = delete;

  void add_topic(char const* topic) { m_topics.push_back(topic); }
  void send_message();

 private slots:
  void messageReceived(QList<QByteArray> const& message);

 private:
  nzmqt::ZMQContext *m_zmqContext;
  nzmqt::ZMQSocket *m_socket;
  std::vector<std::string> m_topics;
};
#endif

#endif // BROKERCLIENT_H
