// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QThread>
#include <QMutex>
#include <broker/utils.h>
#include <broker/broker_server.pb.h>
#include "topicdata.pb.h"

namespace Ui {
class Window;
}

namespace broker {
class Client;
}

class QTimer;

class WorkerThread final : public QThread {
  Q_OBJECT

 public:
  explicit WorkerThread(QObject *parent = nullptr);
  void run() override;
};

class Window : public QWidget {
  Q_OBJECT

 public:
  explicit Window(QWidget *parent = nullptr);
  ~Window();

  void update(broker::messages_t const &a_messages);
  broker::Client* client() const { return m_client; }

 private slots:
  void update();

 private:
  Ui::Window *m_ui{};
  broker::Client* m_client{};
  WorkerThread *m_thread{};
  QTimer *m_updateTimer{};
  TopicData m_values[3];
  QMutex m_mutex{};
};

#endif // WINDOW_H
