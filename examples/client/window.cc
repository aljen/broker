// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "window.h"
#include "ui_window.h"

#include <iostream>

#include <QDebug>
#include <QTimer>

#include <broker/client.h>
#include <broker/broker_server.pb.h>

WorkerThread::WorkerThread(QObject *parent)
  : QThread{parent}
{}

void WorkerThread::run()
{
  qDebug() << "Start thread";
  Window *window{ static_cast<Window*>(parent()) };
  broker::Client *const client{ window->client() };
  bool done{};
  while (!done) window->update(client->receive());
  qDebug() << "End thread";
}

Window::Window(QWidget *parent)
  : QWidget{parent}
  , m_ui{new Ui::Window}
  , m_client{ broker::client() }
  , m_thread{new WorkerThread{this}}
  , m_updateTimer{new QTimer{this}}
  , m_values{}
{
  m_ui->setupUi(this);

//  m_ui->topic1->setSuffix(" [bar]");
//  m_ui->topic1->setValueFont(font());
//  m_ui->topic1->setValueOffset(-20.);
//  m_ui->topic1->setDigitOffset(100);
//  m_ui->topic1->resize(120, 120);
  m_ui->topic1->setMinimum(0.0);
  m_ui->topic1->setMaximum(120.0);
  m_ui->topic1->setNominal(0.0);
  m_ui->topic1->setCritical(90.0);
  m_ui->topic1->setValue(12.0);

  m_ui->topic2->setMinimum(0.0);
  m_ui->topic2->setMaximum(120.0);
  m_ui->topic2->setNominal(0.0);
  m_ui->topic2->setCritical(90.0);

  m_ui->topic3->setMinimum(0.0);
  m_ui->topic3->setMaximum(120.0);
  m_ui->topic3->setNominal(0.0);
  m_ui->topic3->setCritical(90.0);

  connect(m_thread, &WorkerThread::finished, m_thread, &QObject::deleteLater);

  m_updateTimer->setInterval(33);
  m_updateTimer->start();
  connect(m_updateTimer, SIGNAL(timeout()), this, SLOT(update()));

  m_client->connect("tcp://10.0.0.2:1234");

  broker::topics_t const topics{"Topic1", "Topic3", "Topic2"};
  m_client->subscribe(topics);
  update(m_client->request(topics[0]));
  update(m_client->request(topics[1]));
  update(m_client->request(topics[2]));

  m_thread->start();
}

Window::~Window()
{
  delete m_ui;
}

void Window::update()
{
  m_mutex.lock();
  m_ui->topic1->setValue(m_values[0].value());
  m_ui->topic1->setCritical(m_values[0].target());
  m_ui->value1->setText(QString("%1/%2/%3").arg(m_values[0].value(), 3, 'f', 2).arg(m_values[0].target(), 3, 'f', 2).arg(m_values[0].speed(), 3, 'f', 2));
  m_ui->topic2->setValue(m_values[1].value());
  m_ui->topic2->setCritical(m_values[1].target());
  m_ui->value2->setText(QString("%1/%2/%3").arg(m_values[1].value(), 3, 'f', 2).arg(m_values[1].target(), 3, 'f', 2).arg(m_values[1].speed(), 3, 'f', 2));
  m_ui->topic3->setValue(m_values[2].value());
  m_ui->topic3->setCritical(m_values[2].target());
  m_ui->value3->setText(QString("%1/%2/%3").arg(m_values[2].value(), 3, 'f', 2).arg(m_values[2].target(), 3, 'f', 2).arg(m_values[2].speed(), 3, 'f', 2));
  m_mutex.unlock();
}

void Window::update(broker::messages_t const &a_messages)
{
  m_mutex.lock();
  size_t const size{a_messages.size()};
//  std::cerr << "size: " << size << "\n";
  broker::Hash replyHash{};
  TopicData topicData{};
  for (size_t i = 0; i < size; i += 2) {
    replyHash.ParseFromString(a_messages[i]);
    topicData.ParseFromString(a_messages[i + 1]);

    switch (replyHash.hash()) {
      case broker::string::hash("Topic1"): m_values[0] = topicData; break;
      case broker::string::hash("Topic2"): m_values[1] = topicData; break;
      case broker::string::hash("Topic3"): m_values[2] = topicData; break;
    }
  }
  m_mutex.unlock();
}
