// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef BROKERWINDOW_H
#define BROKERWINDOW_H

#include <QAbstractItemModel>
#include <QMainWindow>
#include <unordered_map>
#include <iostream>

#include <broker/broker_server.pb.h>
#include <broker/utils.h>
#include <zmq-cpp/nzmqt.hpp>

namespace Ui {
class BrokerWindow;
}

class QStandardItemModel;
class QSortFilterProxyModel;

class BrokerWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit BrokerWindow(QWidget *parent = nullptr);
  ~BrokerWindow();

  broker::PublishResponse processRequest(broker::PublishRequest const &request);
  void processRequest(broker::UnpublishRequest const &request);
  broker::SubscribeResponse processRequest(broker::SubscribeRequest const &request);
  broker::PongResponse processRequest(broker::PingRequest const &request);

 private:
  BrokerWindow(BrokerWindow const &window) = delete;
  void operator=(BrokerWindow const &) = delete;

  void updatePublishers();

 private slots:
  void messageReceived(QList<QByteArray> const &message);
  void on_clearButton_clicked();
  void watchdog();

 private:
  bool removePublisher(QString const &publisher);

 private:
  struct publisher_t {
    std::string topic{};
    broker::ClientInfo info{};
  };
  using publishers_t = std::unordered_map<broker::string::hash_t, publisher_t>;
  Ui::BrokerWindow *m_ui{};
  QTimer *m_watchdog{};
  QStandardItemModel *m_topicsModel{};
  QStandardItemModel *m_serversModel{};
  QSortFilterProxyModel *m_topicsSortModel{};
  QSortFilterProxyModel *m_serversSortModel{};
  nzmqt::ZMQContext *m_zmqContext{};
  nzmqt::ZMQSocket *m_socket{};
  nzmqt::ZMQSocket *m_publish{};
  publishers_t m_publishers{};
  QMap<QString, qint64> m_alives{};
};

#endif // BROKERWINDOW_H
