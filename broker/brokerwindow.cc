// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <QDebug>
#include <QTimer>
#include <iostream>
#include "brokerwindow.h"
#ifdef WIN32
# include <Shlwapi.h>
# include <windows.h>
# include <process.h>
#else
# include <unistd.h>
#endif
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include "ui_brokerwindow.h"

template<typename Container, typename Predicate>
bool erase_if(Container &container, Predicate const &predicate)
{
  bool erased{};
  for (auto iterator = std::begin(container); iterator != std::end(container);) {
    if (predicate(*iterator)) {
      erased |= true;
      iterator = container.erase(iterator);
    } else
      ++iterator;
  }
  return erased;
}

BrokerWindow::BrokerWindow(QWidget *parent)
  : QMainWindow(parent)
  , m_ui{ new Ui::BrokerWindow }
  , m_watchdog{ new QTimer{ this } }
  , m_topicsModel{ new QStandardItemModel{ 0, 6, this } }
  , m_serversModel{ new QStandardItemModel{ 0, 6, this } }
  , m_topicsSortModel{ new QSortFilterProxyModel }
  , m_serversSortModel{ new QSortFilterProxyModel }
  , m_zmqContext{ nzmqt::createDefaultContext(this) }
  , m_socket{ m_zmqContext->createSocket(nzmqt::ZMQSocket::TYP_REP, this) }
  , m_publish{ m_zmqContext->createSocket(nzmqt::ZMQSocket::TYP_PUB, this) }
  , m_publishers{}
{
  m_ui->setupUi(this);

  m_topicsModel->setHeaderData(0, Qt::Horizontal, "Topic");
  m_topicsModel->setHeaderData(1, Qt::Horizontal, "Hash");
  m_topicsModel->setHeaderData(2, Qt::Horizontal, "Hostname");
  m_topicsModel->setHeaderData(3, Qt::Horizontal, "PubPort");
  m_topicsModel->setHeaderData(4, Qt::Horizontal, "ReqPort");
  m_topicsModel->setHeaderData(5, Qt::Horizontal, "Application");
  m_topicsSortModel->setSourceModel(m_topicsModel);

  m_ui->topicsView->setModel(m_topicsSortModel);
  m_ui->topicsView->setAlternatingRowColors(true);
  m_ui->topicsView->setSortingEnabled(true);
  m_ui->topicsView->horizontalHeader()->setStretchLastSection(true);
  m_ui->topicsView->resizeColumnsToContents();
  m_ui->topicsView->verticalHeader()->setVisible(false);
  m_ui->topicsView->setColumnWidth(0, 150);
  m_ui->topicsView->setColumnWidth(1, 75);
  m_ui->topicsView->setColumnWidth(2, 150);
  m_ui->topicsView->setColumnWidth(3, 65);
  m_ui->topicsView->setColumnWidth(4, 65);

  m_zmqContext->setObjectName("ZMQ_CONTEXT");
  m_zmqContext->start();

  m_socket->setObjectName("ZMQ_SOCKET");
  char const *const IP = getenv("BROKER_IP");
  char const *const PORT = getenv("BROKER_PORT");
  assert(IP && PORT);

  char buffer[256]{};
  snprintf(buffer, 256, "tcp://%s:%s", IP, PORT);
  m_socket->bindTo(buffer);

  connect(m_socket, &nzmqt::ZMQSocket::messageReceived, this, &BrokerWindow::messageReceived);
  connect(m_watchdog, &QTimer::timeout, this, &BrokerWindow::watchdog);

  m_watchdog->setInterval(33);
  m_watchdog->start();
}

BrokerWindow::~BrokerWindow()
{
  delete m_ui;
}

broker::PublishResponse BrokerWindow::processRequest(broker::PublishRequest const &request)
{
  using broker::PublishResponse;
  using broker::TopicResult;

  PublishResponse response{};

  if (!request.IsInitialized()) {
    std::cerr << "Request is not initialized" << std::endl;
    response.set_result(PublishResponse::NOT_INITIALIZED);
    return response;
  }
  if (!request.topics_size()) {
    std::cerr << "Request is empty" << std::endl;
    response.set_result(PublishResponse::EMPTY);
    return response;
  }

  int const topicsSize = request.topics_size();
  std::cout << "Topics size: " << topicsSize << std::endl;
  bool isPartial{};
  for (int i = 0; i < topicsSize; ++i) {
    std::string const &topic = request.topics(i);
    auto topicResult = response.add_topic_result();
    topicResult->set_topic(topic);

    auto const hash = broker::string::hash(topic.c_str());
    auto search = m_publishers.find(hash);
    if (search == m_publishers.end()) {
      std::cout << "Adding topic: " << topic << std::endl;
      publisher_t publisher{ topic, request.client() };
      m_publishers[hash] = publisher;
      topicResult->set_result(TopicResult::OK);
    } else {
      std::cerr << topic << " is already provided by " << search->second.info.hostname() << ":" << search->second.info.name()
                << ":" << search->second.info.pid() << std::endl;
      topicResult->set_result(TopicResult::ALREADY_PROVIDED);
      *topicResult->mutable_provider() = search->second.info;
      isPartial = true;
    }
  }

  QString const host = QString("%1:%2:%3").arg(request.client().name().c_str())
                                          .arg(request.client().hostname().c_str())
                                          .arg(request.client().publish_port());

  if (m_alives.contains(host))
    m_alives[host] = 0;
  else
    m_alives.insert(host, 0);

  std::cerr << "Request is ok" << std::endl;
  response.set_result(isPartial ? PublishResponse::PARTIAL : PublishResponse::OK);

  updatePublishers();

  return response;
}

void BrokerWindow::processRequest(broker::UnpublishRequest const &request)
{
  if (!request.IsInitialized()) {
    std::cerr << "Request is not initialized" << std::endl;
    return;
  }

  std::cerr << __PRETTY_FUNCTION__ << std::endl;

  QString const host = QString("%1:%2:%3").arg(request.client().name().c_str())
                                          .arg(request.client().hostname().c_str())
                                          .arg(request.client().publish_port());
//  removePublisher(host);
  updatePublishers();
}

broker::SubscribeResponse BrokerWindow::processRequest(broker::SubscribeRequest const &request)
{
  broker::SubscribeResponse response{};
  if (!request.IsInitialized()) {
    std::cerr << "Request is not initialized" << std::endl;
    return response;
  }

  int32_t const size{request.topic_size()};
  std::cerr << "size: " << size << "\n";
  for (int32_t i = 0; i < size; ++i) {
    auto const &topic = request.topic(i);
    broker::string::hash_t const hash{broker::string::hash(topic.c_str())};
    auto found = m_publishers.find(hash);
    auto result = response.add_result();
    result->set_topic(topic);
    if (found != std::end(m_publishers)) {
      result->set_result(broker::TopicResult::OK);
      *result->mutable_provider() = found->second.info;
    } else
      result->set_result(broker::TopicResult::NOT_FOUND);
  }

  return response;
}

broker::PongResponse BrokerWindow::processRequest(broker::PingRequest const &request)
{
  using broker::PongResponse;

  if (!request.IsInitialized())
    std::cerr << "Request is not initialized" << std::endl;

  QString const host = QString("%1:%2:%3").arg(request.client().name().c_str())
                                          .arg(request.client().hostname().c_str())
                                          .arg(request.client().publish_port());

  PongResponse response{};

  if (m_alives.contains(host)) {
    //qDebug() << "Ping from " << host;
    response.set_result(PongResponse::PONG);
    m_alives[host] = 0;
  } else {
    response.set_result(PongResponse::RECONNECT);
    std::cerr << "ERROR: Host " << host.toStdString() << " is not registered" << std::endl;
  }

  return response;
}

void BrokerWindow::updatePublishers()
{
  m_topicsModel->removeRows(0, m_topicsModel->rowCount(QModelIndex()), QModelIndex());
  m_topicsModel->setRowCount(static_cast<int>(m_publishers.size()));

  int row{};
  for (auto const &publisher : m_publishers) {
    QString const topic{QString::fromStdString(publisher.second.topic)};
    QString const topic_hash{QString::number(publisher.first, 16)};
    QString const hostname{QString::fromStdString(publisher.second.info.hostname())};
    QString const publish_port{QString::number(publisher.second.info.publish_port())};
    QString const request_port{QString::number(publisher.second.info.request_port())};
    QString const name{QString::fromStdString(publisher.second.info.name())};
    QString const pid{QString::number(publisher.second.info.pid())};
    QString const app{QString("%1:%2").arg(name).arg(pid)};
    m_topicsModel->setData(m_topicsModel->index(row, 0, QModelIndex()), topic);
    m_topicsModel->setData(m_topicsModel->index(row, 1, QModelIndex()), topic_hash);
    m_topicsModel->setData(m_topicsModel->index(row, 2, QModelIndex()), hostname);
    m_topicsModel->setData(m_topicsModel->index(row, 3, QModelIndex()), publish_port);
    m_topicsModel->setData(m_topicsModel->index(row, 4, QModelIndex()), request_port);
    m_topicsModel->setData(m_topicsModel->index(row, 5, QModelIndex()), app);
    row++;
  }

  m_topicsSortModel->sort(m_topicsSortModel->sortColumn(), Qt::AscendingOrder);
}

void BrokerWindow::messageReceived(QList<QByteArray> const& message)
{
  using broker::Hash;
  using broker::PublishRequest;
  using broker::PublishResponse;
  using broker::UnpublishRequest;
  using broker::SubscribeRequest;
  using broker::SubscribeResponse;
  using broker::PingRequest;
  using broker::PongResponse;

//  qDebug() << "[SERVER]" << sender()->objectName();
//  qDebug() << "message.count():" << message.count();
//  qDebug() << "message:" << message;

  broker::Hash hash{};
  std::string hashMsg{message[0].toStdString()};
  hash.ParseFromString(hashMsg);

  QList<QByteArray> responseMsg{};
  std::string responseString{};

  switch (hash.hash()) {
    case SID("PublishRequest"): {
      qDebug() << "type: PUBLISH";

      PublishRequest request{};
      std::string requestMsg{message[1].toStdString()};

      request.ParseFromString(requestMsg);
      qDebug() << request.Utf8DebugString().c_str();

      PublishResponse response{processRequest(request)};
      //qDebug() << response.Utf8DebugString().c_str();

      response.SerializeToString(&responseString);
      break;
    }

    case SID("UnpublishRequest"): {
      qDebug() << "type: UNPUBLISH";

      UnpublishRequest request {};
      std::string requestMsg {message[1].toStdString()};

      request.ParseFromString(requestMsg);
      qDebug() << request.Utf8DebugString().c_str();

      processRequest(request);
      break;
    }

    case SID("SubscribeRequest"): {
      qDebug() << "type: SUBSCRIBE";

      SubscribeRequest request{};
      broker::message_t requestMsg{ message[1].toStdString() };

      request.ParseFromString(requestMsg);
      qDebug() << "request:";
      qDebug() << request.Utf8DebugString().c_str();

      qDebug() << "parsing request";
      SubscribeResponse response{ processRequest(request) };
      qDebug() << "response";
      qDebug() << response.Utf8DebugString().c_str();

      response.SerializeToString(&responseString);
      break;
    }

    case SID("PingRequest"): {
//      qDebug() << "type: PING";

      PingRequest request{};
      std::string requestMsg{ message[1].toStdString() };

      request.ParseFromString(requestMsg);
//      qDebug() << "request:";
//      qDebug() << request.Utf8DebugString().c_str();

//      qDebug() << "parsing request";
      PongResponse response{ processRequest(request) };
//      qDebug() << "response";
//      qDebug() << response.Utf8DebugString().c_str();

      response.SerializeToString(&responseString);
      break;
    }

    default:
      std::cerr << "Unknown request: " << hash.hash() << std::endl;
      break;
  }

  responseMsg += QByteArray::fromStdString(responseString);
  m_socket->sendMessage(responseMsg);
}

void BrokerWindow::on_clearButton_clicked()
{
  m_publishers.clear();
  m_alives.clear();
  updatePublishers();
}

void BrokerWindow::watchdog()
{
  qint64 const elapsed{ m_watchdog->interval() };

  bool erased{};
  for (auto iterator = m_alives.begin(); iterator != m_alives.end(); ++iterator) {
    iterator.value() += elapsed;
    if (iterator.value() >= 1000) {
      qDebug() << "removing: " << iterator.key() << " -> value: " << iterator.value();
      erased |= removePublisher(iterator.key());
      iterator = m_alives.erase(iterator);
      if (iterator == m_alives.end()) break;
    }
  }
  if (erased) updatePublishers();
}

bool BrokerWindow::removePublisher(QString const &a_publisher)
{
  qDebug() << Q_FUNC_INFO << a_publisher;

  QStringList values{ a_publisher.split(':') };
  std::string appname{ values[0].toStdString() };
  std::string hostname{ values[1].toStdString() };
  uint32_t port{ static_cast<uint32_t>(stoi(values[2].toStdString())) };

  bool const erased{ erase_if(m_publishers, [&](auto &publisher){
    broker::ClientInfo const &info{ publisher.second.info };
    bool const sameHost{ info.hostname() == hostname };
    bool const sameApp{ info.name() == appname };
    bool const samePort{ info.publish_port() == port };
    if (sameApp && sameHost && samePort) return true;
    return false;
  }) };

  QList<QByteArray> notification{};
  std::string msg{};
  SERIALIZE_HASH("ServerDisconnected", msg);
  notification += QByteArray::fromStdString(msg);

  broker::ServerDisconnected disconnected{};
  disconnected.mutable_client()->set_hostname(hostname);
  disconnected.mutable_client()->set_name(appname);
  disconnected.mutable_client()->set_publish_port(port);
  disconnected.SerializeToString(&msg);
  notification += QByteArray::fromStdString(msg);

  m_publish->sendMessage(notification);

  return erased;
}
