// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <cstdio>
#include <cstdlib>
#include <csignal>
#include <chrono>

#include <broker/broker_server.pb.h>
#include <broker/utils.h>
#include <zmq.hpp>

using Clock = std::chrono::high_resolution_clock;
using TimePoint = Clock::time_point;
using Strings = std::vector<std::string>;

struct publisher_t {
  std::string topic{};
  broker::ClientInfo info{};
};
using publishers_t = std::unordered_map<broker::string::hash_t, publisher_t>;

std::shared_ptr<zmq::context_t> g_context{ std::make_shared<zmq::context_t>() };
std::unique_ptr<zmq::socket_t> g_socket{ std::make_unique<zmq::socket_t>(*g_context, ZMQ_REP) };
std::unique_ptr<zmq::socket_t> g_publish{ std::make_unique<zmq::socket_t>(*g_context, ZMQ_PUB) };

publishers_t g_publishers{};
std::map<std::string, TimePoint> g_alives{};

constexpr auto INTERVAL = std::chrono::milliseconds{ 100 };

std::atomic_bool g_done{};
std::atomic_bool g_debug{};

template<typename Container, typename Predicate>
bool erase_if(Container &container, Predicate const &predicate)
{
  bool erased{};
  for (auto iterator = std::begin(container); iterator != std::end(container);) {
    if (predicate(*iterator)) {
      erased |= true;
      iterator = container.erase(iterator);
    } else
      ++iterator;
  }
  return erased;
}

Strings split(std::string const &a_string, char const a_delimeter)
{
  Strings strings{};

  if (a_string.empty())
    return strings;

  std::string input{ a_string }, temp{};

  size_t const SIZE{ input.size() };

  bool appendLast{};
  for (size_t i = 0; i < SIZE; ++i) {
    char const C{ input[i] };
    if (C == a_delimeter) {
      strings.push_back(temp);
      temp.clear();
    } else {
      temp.push_back(C);
      if (i == SIZE - 1)
        appendLast = true;
    }
  }

  if (appendLast)
    strings.push_back(temp);

  return strings;
}

void quit_handler(int const a_signal)
{
  (void)a_signal;
  g_done = true;
}

broker::PublishResponse process_request(broker::PublishRequest const &a_request)
{
  using broker::PublishResponse;
  using broker::TopicResult;

  PublishResponse response{};

  if (!a_request.IsInitialized()) {
    if (g_debug)
      std::cerr << "Request is not initialized" << std::endl;
    response.set_result(PublishResponse::NOT_INITIALIZED);
    return response;
  }
  if (!a_request.topics_size()) {
    if (g_debug)
      std::cerr << "Request is empty" << std::endl;
    response.set_result(PublishResponse::EMPTY);
    return response;
  }

  int const topicsSize = a_request.topics_size();
  if (g_debug)
    std::cerr << "Topics size: " << topicsSize << std::endl;
  bool isPartial{};
  for (int i = 0; i < topicsSize; ++i) {
    std::string const &topic = a_request.topics(i);
    auto topicResult = response.add_topic_result();
    topicResult->set_topic(topic);

    auto const hash = broker::string::hash(topic.c_str());
    auto search = g_publishers.find(hash);
    if (search == g_publishers.end()) {
      if (g_debug)
        std::cerr << "Adding topic: " << topic << std::endl;
      publisher_t publisher{ topic, a_request.client() };
      g_publishers[hash] = publisher;
      topicResult->set_result(TopicResult::OK);
    } else {
      if (g_debug)
        std::cerr << topic << " is already provided by " << search->second.info.hostname()
                  << ":" << search->second.info.name()
                  << ":" << search->second.info.pid() << std::endl;
      topicResult->set_result(TopicResult::ALREADY_PROVIDED);
      *topicResult->mutable_provider() = search->second.info;
      isPartial = true;
    }
  }

  auto const &CLIENT = a_request.client();
  auto const HOST = CLIENT.name() + ":" + CLIENT.hostname() + ":" + std::to_string(CLIENT.publish_port());
  g_alives[HOST] = Clock::now();

  if (g_debug)
    std::cerr << "Request is ok" << std::endl;
  response.set_result(isPartial ? PublishResponse::PARTIAL : PublishResponse::OK);

  return response;
}

void process_request(broker::UnpublishRequest const &a_request)
{
  if (!a_request.IsInitialized()) {
    if (g_debug)
      std::cerr << "Request is not initialized" << std::endl;
    return;
  }

  if (g_debug)
    std::cerr << __PRETTY_FUNCTION__ << std::endl;

//  QString const host = QString("%1:%2:%3").arg(request.client().name().c_str())
//                                          .arg(request.client().hostname().c_str())
//                                          .arg(request.client().publish_port());
//  removePublisher(host);
}

broker::SubscribeResponse process_request(broker::SubscribeRequest const &a_request)
{
  broker::SubscribeResponse response{};
  if (!a_request.IsInitialized()) {
    if (g_debug)
      std::cerr << "Request is not initialized" << std::endl;
    return response;
  }

  int32_t const size{a_request.topic_size()};
  if (g_debug)
    std::cerr << "size: " << size << "\n";
  for (int32_t i = 0; i < size; ++i) {
    auto const &topic = a_request.topic(i);
    broker::string::hash_t const hash{broker::string::hash(topic.c_str())};
    auto found = g_publishers.find(hash);
    auto result = response.add_result();
    result->set_topic(topic);
    if (found != std::end(g_publishers)) {
      result->set_result(broker::TopicResult::OK);
      *result->mutable_provider() = found->second.info;
    } else
      result->set_result(broker::TopicResult::NOT_FOUND);
  }

  return response;
}

broker::PongResponse process_request(broker::PingRequest const &a_request)
{
  using broker::PongResponse;

  if (!a_request.IsInitialized()) {
    if (g_debug)
      std::cerr << "Request is not initialized" << std::endl;
  }

  auto const &CLIENT = a_request.client();
  auto const HOST = CLIENT.name() + ":" + CLIENT.hostname() + ":" + std::to_string(CLIENT.publish_port());

  PongResponse response{};

  if (g_alives.count(HOST)) {
    response.set_result(PongResponse::PONG);
    g_alives.at(HOST) = Clock::now();
  } else {
    response.set_result(PongResponse::RECONNECT);
    if (g_debug)
      std::cerr << "ERROR: Host " << HOST << " is not registered" << std::endl;
  }

  return response;
}

void handle_message_received(broker::messages_t const& a_message)
{
  using broker::Hash;
  using broker::PublishRequest;
  using broker::PublishResponse;
  using broker::UnpublishRequest;
  using broker::SubscribeRequest;
  using broker::SubscribeResponse;
  using broker::PingRequest;
  using broker::PongResponse;

//  qDebug() << "[SERVER]" << sender()->objectName();
//  qDebug() << "message.count():" << message.count();
//  qDebug() << "message:" << message;

  broker::Hash hash{};
  auto const &HASH_MESSAGE = a_message[0];
  hash.ParseFromString(HASH_MESSAGE);

  std::string responseString{};

  switch (hash.hash()) {
    case SID("PublishRequest"): {
      if (g_debug)
        std::cerr << "type: PUBLISH" << std::endl;

      PublishRequest request{};
      auto const &REQUEST_MESSAGE = a_message[1];

      request.ParseFromString(REQUEST_MESSAGE);

      if (g_debug)
        std::cerr << request.Utf8DebugString() << std::endl;

      auto const RESPONSE = process_request(request);
      //qDebug() << response.Utf8DebugString().c_str();

      RESPONSE.SerializeToString(&responseString);
      break;
    }

    case SID("UnpublishRequest"): {
      if (g_debug)
        std::cerr << "type: UNPUBLISH" << std::endl;

      UnpublishRequest request{};
      auto const &REQUEST_MESSAGE = a_message[1];

      request.ParseFromString(REQUEST_MESSAGE);
      if (g_debug)
        std::cerr << request.Utf8DebugString() << std::endl;

      process_request(request);
      break;
    }

    case SID("SubscribeRequest"): {
      if (g_debug)
        std::cerr << "type: SUBSCRIBE" << std::endl;

      SubscribeRequest request{};
      auto const &REQUEST_MESSAGE = a_message[1];

      request.ParseFromString(REQUEST_MESSAGE);
      auto const RESPONSE = process_request(request);
      if (g_debug) {
        std::cerr << "request:" << std::endl;
        std::cerr << request.Utf8DebugString() << std::endl;
        std::cerr << "response" << std::endl;
        std::cerr << RESPONSE.Utf8DebugString() << std::endl;
      }

      RESPONSE.SerializeToString(&responseString);
      break;
    }

    case SID("PingRequest"): {
//      qDebug() << "type: PING";

      PingRequest request{};
      auto const &REQUEST_MESSAGE = a_message[1];

      request.ParseFromString(REQUEST_MESSAGE);
//      qDebug() << "request:";
//      qDebug() << request.Utf8DebugString().c_str();

//      qDebug() << "parsing request";
      auto const RESPONSE = process_request(request);
//      qDebug() << "response";
//      qDebug() << response.Utf8DebugString().c_str();

      RESPONSE.SerializeToString(&responseString);
      break;
    }

    default:
      if (g_debug)
        std::cerr << "Unknown request: " << hash.hash() << std::endl;
      break;
  }

  g_socket->send(responseString.c_str(), responseString.size());
}

bool remove_publisher(std::string const &a_publisher)
{
  if (g_debug)
    std::cerr << __PRETTY_FUNCTION__ << " publisher: " << a_publisher << std::endl;

  auto VALUES = split(a_publisher, ':');
  auto const &APPNAME = VALUES[0];
  auto const &HOSTNAME = VALUES[1];
  auto const PORT = static_cast<uint32_t>(stoi(VALUES[2]));

  bool const ERASED = erase_if(g_publishers, [&](auto &publisher) {
    broker::ClientInfo const &info{ publisher.second.info };
    auto const SAME_HOST = info.hostname() == HOSTNAME;
    auto const SAME_APP = info.name() == APPNAME;
    auto const SAME_PORT = info.publish_port() == PORT;
    if (SAME_APP && SAME_HOST && SAME_PORT) return true;
    return false;
  });

//  std::string notification{};

//  std::string msg{};
//  SERIALIZE_HASH("ServerDisconnected", msg);
//  notification += QByteArray::fromStdString(msg);

//  broker::ServerDisconnected disconnected{};
//  disconnected.mutable_client()->set_hostname(hostname);
//  disconnected.mutable_client()->set_name(appname);
//  disconnected.mutable_client()->set_publish_port(port);
//  disconnected.SerializeToString(&msg);
//  notification += QByteArray::fromStdString(msg);

//  m_publish->sendMessage(notification);

  return ERASED;
}

void handle_watchdog()
{
  auto const NOW = Clock::now();

  for (auto iterator = g_alives.begin(); iterator != g_alives.end(); ++iterator) {
    if (NOW - iterator->second>= std::chrono::seconds{ 1 }) {
      if (g_debug)
        std::cerr << "removing: " << iterator->first << std::endl;
      remove_publisher(iterator->first);
      iterator = g_alives.erase(iterator);
      if (iterator == g_alives.end()) break;
    }
  }
}

int main(int argc, char **argv)
{
  (void)argc;
  (void)argv;

  GOOGLE_PROTOBUF_VERIFY_VERSION;

  struct sigaction sigIntTermHandler{};
  sigIntTermHandler.sa_handler = quit_handler;
  sigfillset(&sigIntTermHandler.sa_mask);
  sigIntTermHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntTermHandler, nullptr);
  sigaction(SIGTERM, &sigIntTermHandler, nullptr);

  char const *const IP = getenv("BROKER_IP");
  char const *const PORT = getenv("BROKER_PORT");
  assert(IP && PORT);
  g_debug.store(getenv("BROKER_DEBUG") != nullptr);

  std::cerr << "Debug output enabled: " << g_debug << std::endl;

  char buffer[256]{};
  snprintf(buffer, 256, "tcp://%s:%s", IP, PORT);
  g_socket->bind(buffer);

  auto last = Clock::now();

  while (!g_done) {
    auto const NOW = Clock::now();
    auto const DIFF = NOW - last;
    if (DIFF > INTERVAL) {
      handle_watchdog();
    }

    auto const MESSAGES = broker::receive(g_socket.get(), ZMQ_DONTWAIT);
    auto const EMPTY = MESSAGES.size() == 1 && MESSAGES[0].size() == 0;
    if (!EMPTY)
      handle_message_received(MESSAGES);
  }

  std::cerr << "Shutting down" << std::endl;

  g_socket->close();
  g_publish->close();

  google::protobuf::ShutdownProtobufLibrary();

  return EXIT_SUCCESS;
}
