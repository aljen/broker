cmake_minimum_required(VERSION 3.10)

project(brokerd)

set(CMAKE_CXX_STANDARD 17)

if (NOT win32)
  find_package(ZLIB)
endif ()

if (NOT BROKER_USE_OLD_CXX_ABI)
  find_package(Protobuf REQUIRED)
endif()

find_package(PkgConfig REQUIRED)

pkg_check_modules(ZEROMQ REQUIRED libzmq)

add_executable(brokerd
  ${SERVICES}
  main.cc
  )

target_include_directories(brokerd
  SYSTEM PRIVATE ${VENDOR_ROOT}
  )

target_link_libraries(brokerd
  PRIVATE Broker
  PRIVATE $<$<BOOL:${BROKER_USE_OLD_CXX_ABI}>:libprotobuf>
#  protobuf
  ${ZEROMQ_LDFLAGS}
  )

include(GNUInstallDirs)

install(TARGETS brokerd
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  )
