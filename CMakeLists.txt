cmake_minimum_required(VERSION 3.10)

project(Broker
  VERSION 18.08.15
  DESCRIPTION "Broker"
  LANGUAGES C CXX
)

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

include(BuildOptions)
include(BuildLocation)
include(BuildType)
include(CompilerType)
include(CompilerFlags)
include(GetRepoInfo)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)

option(BROKER_ENABLE_ALL_WARNINGS "Enable all warnings" OFF)
option(BROKER_TREAT_WARNINGS_AS_ERRORS "Treat warnings as errors" OFF)
option(BROKER_USE_OLD_CXX_ABI "Use _GLIBCXX_USE_CXX11_ABI" OFF)
if (NOT BROKER_USE_OLD_CXX_ABI)
  option(BROKER_BUILD_EXAMPLES "Build examples" OFF)
endif ()

if (GCC OR CLANG)
  option(BROKER_BUILD_NATIVE "Build native" OFF)
  option(BROKER_USE_CLANG_TIDY "Use clang-tidy" OFF)
  option(BROKER_USE_SANITIZERS "Use sanitizers" OFF)
endif ()

set(PROJECT_ROOT ${PROJECT_SOURCE_DIR})
set(VENDOR_ROOT ${PROJECT_SOURCE_DIR}/vendor)
set(COMMON_ROOT ${PROJECT_SOURCE_DIR}/common)
set(ZMQCPP_ROOT ${VENDOR_ROOT}/zmq-cpp)
set(ZMQCPP_HEADER ${ZMQCPP_ROOT}/zmq.hpp)
set(NZMQT_SOURCES ${ZMQCPP_ROOT}/nzmqt.hpp ${ZMQCPP_ROOT}/nzmqt_global.hpp ${ZMQCPP_ROOT}/nzmqt.cc)
set(SYSTEMD ${PROJECT_ROOT}/systemd.services)
set(SERVICES ${SYSTEMD}/brokerd.service ${SYSTEMD}/overrides/brokerd.conf)

if (BROKER_USE_OLD_CXX_ABI)
  set(protobuf_BUILD_TESTS OFF CACHE BOOL "Option" FORCE)
  set(protobuf_BUILD_EXAMPLES OFF CACHE BOOL "Option" FORCE)
  set(protobuf_BUILD_PROTOC_BINARIES OFF CACHE BOOL "Option" FORCE)
  set(protobuf_BUILD_SHARED_LIBS OFF CACHE BOOL "Option" FORCE)
  add_subdirectory(vendor/protobuf/cmake EXCLUDE_FROM_ALL)
  target_compile_definitions(libprotobuf PUBLIC -D_GLIBCXX_USE_CXX11_ABI=0)
  target_compile_definitions(libprotobuf-lite PUBLIC -D_GLIBCXX_USE_CXX11_ABI=0)
endif ()
add_subdirectory(libbroker)

if (NOT BROKER_USE_OLD_CXX_ABI)
#  add_subdirectory(broker)
  add_subdirectory(brokerd)

  if (BROKER_BUILD_EXAMPLES)
    add_subdirectory(examples/client)
    add_subdirectory(examples/server)
  endif  ()
endif ()
