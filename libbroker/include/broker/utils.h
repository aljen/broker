// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once
#ifndef LIBBROKER_UTILS_H
#define LIBBROKER_UTILS_H

#include <stdint.h>
#include <string>
#include <vector>

#include <broker/strings.h>

#define BROKER_COMPILER_CLANG 0
#define BROKER_COMPILER_GCC 0
#define BROKER_COMPILER_MSVC 0

#define BROKER_PLATFORM_ANDROID 0
#define BROKER_PLATFORM_IOS 0
#define BROKER_PLATFORM_LINUX 0
#define BROKER_PLATFORM_OSX 0
#define BROKER_PLATFORM_WINDOWS 0

#define BROKER_CPU_ARM 0
#define BROKER_CPU_X86 0

#define BROKER_ARCH_32 0
#define BROKER_ARCH_64 0

#define BROKER_DEBUG 0

#if defined(_MSC_VER)
# undef BROKER_COMPILER_MSVC
# define BROKER_COMPILER_MSVC 1
#elif defined(__clang__)
# undef BROKER_COMPILER_CLANG
# define BROKER_COMPILER_CLANG 1
#elif defined(__GNUC__)
# undef BROKER_COMPILER_GCC
# define BROKER_COMPILER_GCC 1
#else
# error "UNKNOWN COMPILER!"
#endif

#if defined(__ANDROID__)
# undef BROKER_PLATFORM_ANDROID
# define BROKER_PLATFORM_ANDROID 1
#elif defined(__ENVIRONMENT_IPHONE_OS_VERSION_MIN_REQUIRED__)
# undef BROKER_PLATFORM_IOS
# define BROKER_PLATFORM_IOS 1
#elif defined(__linux__)
# undef BROKER_PLATFORM_LINUX
# define BROKER_PLATFORM_LINUX 1
#elif defined(__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__)
# undef BROKER_PLATFORM_OSX
# define BROKER_PLATFORM_OSX 1
#elif defined(_WIN64) || defined(_WIN32)
# undef BROKER_PLATFORM_WINDOWS
# define BROKER_PLATFORM_WINDOWS 1
#else
# error "UNKNOWN PLATFORM!"
#endif

#if defined(__arm__)
# undef BROKER_CPU_ARM
# define BROKER_CPU_ARM 1
#elif defined(_M_IX86) || defined(_M_X64) || defined(__i386__) || defined(__x86_64__)
# undef BROKER_CPU_X86
# define BROKER_CPU_X86 1
#endif

#if defined(__x86_64__) || defined(_M_X64) || defined(__64BIT__)
# undef BROKER_ARCH_64
# define BROKER_ARCH_64 1
#else
# undef BROKER_ARCH_32
# define BROKER_ARCH_32 1
#endif

#if BROKER_PLATFORM_WINDOWS
# if defined(BROKER_SHARED)
#  if defined(BROKER_EXPORTS)
#   define BROKER_API __declspec(dllexport)
#  else
#   define BROKER_API __declspec(dllimport)
#  endif
# else
#  define BROKER_API
# endif
#else
# define BROKER_API __attribute__((visibility("default")))
#endif

#if BROKER_COMPILER_MSVC
# define BROKER_FUNCTION __FUNCSIG__
#else
# define BROKER_FUNCTION __PRETTY_FUNCTION__
#endif

#if !defined(NDEBUG)
# undef BROKER_DEBUG
# define BROKER_DEBUG 1
#endif

#define SERIALIZE_HASH(str, msg)              \
  do {                                        \
    broker::Hash hash{};                      \
    hash.set_hash(broker::string::hash(str)); \
    hash.SerializeToString(&msg);             \
  } while (0)

#define SERIALIZE_HASH2(str, hash, msg)       \
  do {                                        \
    hash.set_hash(broker::string::hash(str)); \
    hash.SerializeToString(&msg);             \
  } while (0)

namespace zmq {
class socket_t;
}

namespace broker {

using signals_t = std::vector<int32_t>;
using message_t = std::string;
using messages_t = std::vector<message_t>;
using topic_t = std::string;
using topics_t = std::vector<topic_t>;

BROKER_API void catch_em_all(signals_t const &a_quit, signals_t const &a_debug = signals_t(),
                          signals_t const &a_ignore = signals_t());
BROKER_API std::string gethostname();
BROKER_API std::string gethostbyaddr(std::string const &ip);

BROKER_API broker::messages_t receive(zmq::socket_t *const a_socket, int32_t const &a_flags = 0);

} // namespace broker

#endif // LIBBROKER_UTILS_H
