// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once
#ifndef LIBBROKER_CLIENT_H
#define LIBBROKER_CLIENT_H

#include <broker/utils.h>

namespace zmq {
class socket_t;
}

namespace broker {

class Client;
BROKER_API Client *client();

#ifndef ZMQ_DONTWAIT
#define ZMQ_DONTWAIT 1
#endif
#ifndef ZMQ_SNDMORE
#define ZMQ_SNDMORE 2
#endif

class BROKER_API Client {
 public:
  void connect(std::string const &a_broker);

  void subscribe(topic_t const &a_topic) { subscribe(topics_t{ a_topic }); }
  void subscribe(topics_t const &a_topics);

  messages_t request(topic_t const &a_topic);
  messages_t request(const topic_t &a_topic, std::string a_arg);

  messages_t update(topic_t const &a_topic, message_t const &a_data);

  messages_t receive(int32_t const &a_flags = 0) const { return receive(Socket::eSubscribe, a_flags | ZMQ_SNDMORE); }

  void quit();

  ~Client();

 private:
  Client();

  Client(Client const &) = delete;
  Client &operator=(Client const &) = delete;

  enum class Socket { eBroker, eSubscribe, eRequestUpdate };

  zmq::socket_t *getSocket(Socket const &a_socket) const;
  void send(Socket const &a_socket, message_t const &a_message, int32_t const &a_flag = 0);
  void send(Socket const &a_socket, messages_t const &a_messages, int32_t const &a_flag = 0);
  messages_t receive(Socket const &a_socket, int32_t const &a_flags = 0) const;

 private:
  struct pimpl_t;
  pimpl_t *const m_pimpl;

  friend BROKER_API Client *client();
};

} // namespace broker

#endif // LIBBROKER_CLIENT_H
