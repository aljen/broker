// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once
#ifndef LIBBROKER_SERVER_H
#define LIBBROKER_SERVER_H

#include <broker/utils.h>

namespace zmq {
class socket_t;
#ifndef ZMQ_DONTWAIT
#define ZMQ_DONTWAIT 1
#endif
} // namespace zmq

namespace broker {

class Server;
BROKER_API Server &server();

class BROKER_API Server {
 public:
  void bind(std::string const &a_address, uint32_t const &a_publishPort, uint32_t const &a_requestPort);
  void connect(std::string const &a_broker);

  void registerTopics(topic_t const &a_topic) { registerTopics(topics_t{ a_topic }); }
  void registerTopics(topics_t const &a_topics);
  void unregisterTopics();

  void ping();

  bool hasIncoming() const;
  messages_t getIncoming() const { return receive(Socket::eIncoming, ZMQ_DONTWAIT); }
  void sendReply(message_t const &a_message = message_t(), int32_t const &a_flag = 0)
  {
    sendReply(messages_t{ a_message }, a_flag);
  }
  void sendReply(messages_t const &a_messages, int32_t const &a_flag = 0)
  {
    send(Socket::eIncoming, a_messages, a_flag);
  }

  void publish(messages_t const &a_messages, int32_t const &a_flag = 0) { send(Socket::ePublish, a_messages, a_flag); }

  void close();

 private:
  Server();
  ~Server();

  Server(Server const &) = delete;
  Server &operator=(Server const &) = delete;

  enum class Socket { eBroker, ePublish, eIncoming };

  zmq::socket_t *getSocket(Socket const &a_socket) const;
  void send(Socket const &a_socket, message_t const &a_message, int32_t const &a_flags = 0);
  void send(Socket const &a_socket, messages_t const &a_messages, int32_t const &a_flags = 0);
  messages_t receive(Socket const &a_socket, int32_t const &a_flags = 0) const;
  void registerTopics(topics_t const &a_topics, bool a_store);

 private:
  struct pimpl_t;
  pimpl_t *const m_pimpl{};

  friend BROKER_API Server &server();
};

} // namespace broker

#endif // LIBBROKER_SERVER_H
