// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <memory>
#include <set>
#include <thread>
#include <unordered_map>
#include <zmq-cpp/zmq.hpp>

#include "broker/client.h"

#include "broker/broker_common.pb.h"
#include "broker/broker_server.pb.h"

//#define TRACE_LOG

namespace broker {

extern std::shared_ptr<zmq::context_t> g_context;

extern void get_client_info(ClientInfo *const a_info);
extern void send(zmq::socket_t *const a_socket, message_t const &a_message, int32_t const &a_flag);
extern void send(zmq::socket_t *const a_socket, messages_t const &a_messages, int32_t const &a_flag);

Client *client()
{
  //  static Client s_client{};
  //  return s_client;
  return new Client{};
}

struct Client::pimpl_t {
  pimpl_t()
    : context{ g_context }
    , broker_socket{ std::make_unique<zmq::socket_t>(*context, ZMQ_REQ) }
    , subscribe_socket{ std::make_unique<zmq::socket_t>(*context, ZMQ_SUB) }
    , request_update_socket{ std::make_unique<zmq::socket_t>(*context, ZMQ_REQ) }
    , socket{ std::make_unique<zmq::socket_t>(*context, ZMQ_REQ) }
    , map{}
  {
    int32_t temp{};
    broker_socket->setsockopt(ZMQ_LINGER, &temp, sizeof(temp));
    subscribe_socket->setsockopt(ZMQ_LINGER, &temp, sizeof(temp));
    request_update_socket->setsockopt(ZMQ_LINGER, &temp, sizeof(temp));
    socket->setsockopt(ZMQ_LINGER, &temp, sizeof(temp));
    //    temp = 1000;
    //    broker_socket->setsockopt(ZMQ_SNDTIMEO, &temp, sizeof(temp));
    //    broker_socket->setsockopt(ZMQ_RCVTIMEO, &temp, sizeof(temp));
  }

  std::shared_ptr<zmq::context_t> context{};
  std::unique_ptr<zmq::socket_t> broker_socket{};
  std::unique_ptr<zmq::socket_t> subscribe_socket{};
  std::unique_ptr<zmq::socket_t> request_update_socket{};
  std::unique_ptr<zmq::socket_t> socket{};
  std::unordered_map<broker::string::hash_t, ClientInfo> map{};
  bool quit_requested{};
};

Client::Client()
  : m_pimpl{ new pimpl_t }
{
}

Client::~Client()
{
  std::cerr << __PRETTY_FUNCTION__ << ">\n";
  m_pimpl->quit_requested = true;

  //  int32_t const linger{};
  //  m_pimpl->broker_socket->setsockopt(ZMQ_LINGER, &linger, sizeof(linger));
  //  m_pimpl->subscribe_socket->setsockopt(ZMQ_LINGER, &linger, sizeof(linger));
  //  m_pimpl->request_update_socket->setsockopt(ZMQ_LINGER, &linger, sizeof(linger));
  //  m_pimpl->socket->setsockopt(ZMQ_LINGER, &linger, sizeof(linger));

  //  m_pimpl->broker_socket->close();
  //  m_pimpl->subscribe_socket->close();
  //  m_pimpl->request_update_socket->close();
  //  m_pimpl->socket->close();
  //  m_pimpl->context->close();
  //  zmq_term(m_pimpl->context.get());

  delete m_pimpl;
  std::cerr << __PRETTY_FUNCTION__ << "<\n";
}

void Client::quit()
{
  std::cerr << "Quit>\n";
  m_pimpl->quit_requested = true;

  int32_t temp{};
  m_pimpl->broker_socket->setsockopt(ZMQ_LINGER, &temp, sizeof(temp));
  m_pimpl->subscribe_socket->setsockopt(ZMQ_LINGER, &temp, sizeof(temp));
  m_pimpl->request_update_socket->setsockopt(ZMQ_LINGER, &temp, sizeof(temp));
  m_pimpl->socket->setsockopt(ZMQ_LINGER, &temp, sizeof(temp));

  m_pimpl->broker_socket->close();
  m_pimpl->subscribe_socket->close();
  m_pimpl->request_update_socket->close();
  //  m_pimpl->socket->close();
  //  m_pimpl->context->close();
  std::cerr << "Quit<\n";
}

void Client::connect(std::string const &a_broker)
{
  m_pimpl->broker_socket->connect(a_broker);
}

void Client::subscribe(topics_t const &a_topics)
{
  using namespace std::chrono_literals;
  if (a_topics.empty()) return;

  topics_t to_subscribe{ a_topics };
  int32_t const timeout{ 3 };

  for (auto const &topic : a_topics) {
    message_t sub{};
    SERIALIZE_HASH(topic.c_str(), sub);
    m_pimpl->subscribe_socket->setsockopt(ZMQ_SUBSCRIBE, &sub[0], sub.size());
  }

  while (!to_subscribe.empty()) {
    if (m_pimpl->quit_requested) return;
    message_t message{}, hashMsg{};

    SERIALIZE_HASH("SubscribeRequest", hashMsg);

    SubscribeRequest request{};
    for (auto &topic : to_subscribe) request.add_topic(topic);

    request.SerializeToString(&message);

    messages_t messages{};

    send(Socket::eBroker, hashMsg, ZMQ_SNDMORE);
    send(Socket::eBroker, message);

    do {
      if (m_pimpl->quit_requested) return;
      messages = receive(Socket::eBroker, ZMQ_DONTWAIT);
      std::this_thread::sleep_for(100ms);
    } while (messages.size() == 1 && messages[0].empty());
    assert(messages.size() == 1 && !messages[0].empty());

    if (m_pimpl->quit_requested) return;

    SubscribeResponse response{};
    response.ParseFromString(messages[0]);
    int32_t size{ response.result_size() };
    for (int32_t i = 0; i < size; ++i) {
      auto const &result = response.result(i);
      if (result.result() == broker::TopicResult::NOT_FOUND) continue;
      broker::string::hash_t const &hash{ broker::string::hash(result.topic().c_str()) };
      to_subscribe.erase(std::remove_if(std::begin(to_subscribe), std::end(to_subscribe),
                                        [&hash](auto &t) { return hash == broker::string::hash(t.c_str()); }));
      m_pimpl->map[hash] = result.provider();
    }

    if (to_subscribe.size()) {
      std::cerr << "Still waiting to subscribe topics ";
      for (auto &topic : to_subscribe) std::cerr << topic << ", ";
      std::cerr << "retry in " << timeout << " seconds.." << std::endl;
      std::this_thread::sleep_for(std::chrono::seconds(timeout));
    }
  }

  std::set<std::string> servers{};
  for (auto const &entry : m_pimpl->map) {
    char buffer[256] {};
    snprintf(buffer, 256, "tcp://%s:%d", entry.second.hostname().c_str(), entry.second.publish_port());
//    std::cout << __FILE__ << ":" << __LINE__ << ": " << buffer << std::endl;
    std::string const HOST{ buffer };
    servers.insert(HOST);
  }

  for (auto const &host : servers) {
#ifdef TRACE_LOG
    std::cerr << "[P] Connecting to " << host << std::endl;
#endif
    m_pimpl->subscribe_socket->connect(host);
  }
}

messages_t Client::request(const topic_t &a_topic, std::string a_arg)
{
  messages_t messages{};
  if (a_topic.empty()) return messages;

  std::unordered_map<std::string, topics_t> map{};

  broker::string::hash_t const hash{ broker::string::hash(a_topic.c_str()) };

  if (m_pimpl->map.find(hash) == m_pimpl->map.end()) {
    std::cerr << "Error: Can't find topic '" << a_topic << "'\n";
    return messages;
  }

  char buffer[256]{};
  snprintf(buffer, 256, "tcp://%s:%d", m_pimpl->map[hash].hostname().c_str(), m_pimpl->map[hash].request_port());
  std::string const requestHost{ buffer };
  auto found = map.find(requestHost);
  if (found != std::end(map)) {
    found->second.push_back(a_topic);
  } else {
    map[requestHost].push_back(a_topic);
  }

  for (auto const &entry : map) {
    message_t message{};

#ifdef TRACE_LOG
    std::cerr << "Requesting data from host: " << entry.first << " for topics: ";
#endif

    DataRequest request{};
    for (auto const &topic : entry.second) {
#ifdef TRACE_LOG
      std::cerr << "topic "
                << " " << topic;
#endif
      request.set_topic(topic);
      request.set_argument(a_arg);
    }
    std::cerr << std::endl;

#ifdef TRACE_LOG
    std::cerr << "Connecting to " << entry.first << "..." << std::endl;
#endif
    m_pimpl->request_update_socket->connect(entry.first);

#ifdef TRACE_LOG
    std::cerr << "Sending request..." << std::endl;
#endif
    SERIALIZE_HASH("DataRequest", message);
    send(Socket::eRequestUpdate, message, ZMQ_SNDMORE);

    request.SerializeToString(&message);
    send(Socket::eRequestUpdate, message);
    // send(Socket::eRequestUpdate, a_data);

#ifdef TRACE_LOG
    std::cerr << "Receiving data..." << std::endl;
#endif
    messages_t temp{ receive(Socket::eRequestUpdate) };
#ifdef TRACE_LOG
    std::cerr << "Received data size: " << temp.size() << std::endl;
#endif
    messages.insert(messages.end(), std::make_move_iterator(temp.begin()), std::make_move_iterator(temp.end()));

    m_pimpl->request_update_socket->disconnect(entry.first);
  }

#ifdef TRACE_LOG
  std::cerr << "Received messages size: " << messages.size() << std::endl;
#endif

  return messages;
}

messages_t Client::request(topic_t const &a_topic)
{
  messages_t messages{};

  if (a_topic.empty()) return messages;

  std::unordered_map<std::string, topics_t> map{};

  broker::string::hash_t const hash{ broker::string::hash(a_topic.c_str()) };

  if (m_pimpl->map.find(hash) == m_pimpl->map.end()) {
    std::cerr << "Error: Can't find topic '" << a_topic << "'\n";
    return messages;
  }

  char buffer[256]{};
  snprintf(buffer, 256, "tcp://%s:%d", m_pimpl->map[hash].hostname().c_str(), m_pimpl->map[hash].request_port());
  std::string const requestHost{ buffer };
  auto found = map.find(requestHost);
  if (found != std::end(map)) {
    found->second.push_back(a_topic);
  } else {
    map[requestHost].push_back(a_topic);
  }

  for (auto const &entry : map) {
    message_t message{};

#ifdef TRACE_LOG
    std::cerr << "Requesting data from host: " << entry.first << " for topics: ";
#endif

    DataRequest request{};
    for (auto const &topic : entry.second) {
#ifdef TRACE_LOG
      std::cerr << topic << " ";
#endif
      request.set_topic(topic);
    }
    std::cerr << std::endl;

#ifdef TRACE_LOG
    std::cerr << "Connecting to " << entry.first << "..." << std::endl;
#endif
    m_pimpl->request_update_socket->connect(entry.first);

#ifdef TRACE_LOG
    std::cerr << "Sending request..." << std::endl;
#endif
    SERIALIZE_HASH("DataRequest", message);
    send(Socket::eRequestUpdate, message, ZMQ_SNDMORE);

    request.SerializeToString(&message);
    send(Socket::eRequestUpdate, message);

#ifdef TRACE_LOG
    std::cerr << "Receiving data..." << std::endl;
#endif
    messages_t temp{ receive(Socket::eRequestUpdate) };
#ifdef TRACE_LOG
    std::cerr << "Received data size: " << temp.size() << std::endl;
#endif
    messages.insert(messages.end(), std::make_move_iterator(temp.begin()), std::make_move_iterator(temp.end()));

    m_pimpl->request_update_socket->disconnect(entry.first);
  }

#ifdef TRACE_LOG
  std::cerr << "Received messages size: " << messages.size() << std::endl;
#endif

  return messages;
}

messages_t Client::update(topic_t const &a_topic, message_t const &a_data)
{
  broker::Hash hash{};
  message_t hashMsg{};
  SERIALIZE_HASH2(a_topic.c_str(), hash, hashMsg);

  if (m_pimpl->map.find(hash.hash()) == m_pimpl->map.end()) {
    std::cerr << "Error: Can't find topic '" << a_topic << "'\n";
    return messages_t{};
  }

  char buffer[256]{};
  snprintf(buffer, 256, "tcp://%s:%d", m_pimpl->map[hash.hash()].hostname().c_str(), m_pimpl->map[hash.hash()].request_port());
  std::string const requestHost{ buffer };
#ifdef TRACE_LOG
  std::cerr << "Updating data for " << a_topic << " at host: " << requestHost << "\n";
  std::cerr << "Connecting..\n";
#endif
  m_pimpl->request_update_socket->connect(requestHost);

  message_t dataHash{};
#ifdef TRACE_LOG
  std::cerr << "Sending update..\n";
#endif
  SERIALIZE_HASH2("DataUpdate", hash, dataHash);
  send(Socket::eRequestUpdate, dataHash, ZMQ_SNDMORE);
  send(Socket::eRequestUpdate, hashMsg, ZMQ_SNDMORE);
  send(Socket::eRequestUpdate, a_data);

#ifdef TRACE_LOG
  std::cerr << "Receiving data..." << std::endl;
#endif
  messages_t reply{ receive(Socket::eRequestUpdate) };
#ifdef TRACE_LOG
  std::cerr << "Received data size: " << reply.size() << std::endl;
#endif

  m_pimpl->request_update_socket->disconnect(requestHost);

  return reply;
}

zmq::socket_t *Client::getSocket(Socket const &a_socket) const
{
  switch (a_socket) {
    case Socket::eBroker: return m_pimpl->broker_socket.get();
    case Socket::eSubscribe: return m_pimpl->subscribe_socket.get();
    case Socket::eRequestUpdate: return m_pimpl->request_update_socket.get();
    default: break;
  }

  return nullptr;
}

void Client::send(Socket const &a_socket, message_t const &a_message, int32_t const &a_flag)
{
  zmq::socket_t *const socket{ getSocket(a_socket) };
  assert(socket);

  broker::send(socket, a_message, a_flag);
}

void Client::send(Socket const &a_socket, messages_t const &a_messages, int32_t const &a_flag)
{
  if (a_messages.empty()) return;

  zmq::socket_t *const socket{ getSocket(a_socket) };
  assert(socket);

  broker::send(socket, a_messages, a_flag);
}

messages_t Client::receive(Socket const &a_socket, int32_t const &a_flags) const
{
  zmq::socket_t *const socket{ getSocket(a_socket) };
  assert(socket);

  return broker::receive(socket, a_flags);
}

} // namespace broker
