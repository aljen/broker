// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <algorithm>
#include <chrono>
#include <memory>
#include <thread>
#include <zmq-cpp/zmq.hpp>

#include "broker/server.h"

#include "broker/broker_server.pb.h"

namespace broker {

extern std::shared_ptr<zmq::context_t> g_context;

extern void get_client_info(ClientInfo *const a_info);
extern void send(zmq::socket_t *const a_socket, message_t const &a_message, int32_t const &a_flag);
extern void send(zmq::socket_t *const a_socket, messages_t const &a_messages, int32_t const &a_flag);

Server &server()
{
  static Server s_server{};
  return s_server;
}

struct Server::pimpl_t {
  pimpl_t()
    : context{ g_context }
    , broker_socket{ std::make_unique<zmq::socket_t>(*context, ZMQ_REQ) }
    , publish_socket{ std::make_unique<zmq::socket_t>(*context, ZMQ_PUB) }
    , incoming_socket{ std::make_unique<zmq::socket_t>(*context, ZMQ_REP) }
    , incoming_poll{ static_cast<void *>(*incoming_socket.get()), 0, ZMQ_POLLIN, 0 }
  {
    int32_t const linger{};
    broker_socket->setsockopt(ZMQ_LINGER, &linger, sizeof(linger));
    publish_socket->setsockopt(ZMQ_LINGER, &linger, sizeof(linger));
    incoming_socket->setsockopt(ZMQ_LINGER, &linger, sizeof(linger));
    //    int32_T const limit{10}
    //    publish_socket->setsockopt(ZMQ_SNDHWM, &limit, sizeof(limit));
  }

  std::shared_ptr<zmq::context_t> context{};
  std::unique_ptr<zmq::socket_t> broker_socket{};
  std::unique_ptr<zmq::socket_t> publish_socket{};
  std::unique_ptr<zmq::socket_t> incoming_socket{};
  zmq::pollitem_t incoming_poll{};
  std::string ip{};
  uint32_t publish_port{};
  uint32_t request_port{};
  topics_t topics{};
  bool registered{};
};

Server::Server()
  : m_pimpl{ new pimpl_t }
{
}

Server::~Server()
{
  delete m_pimpl;
}

void Server::bind(std::string const &a_address, uint32_t const &a_publishPort, uint32_t const &a_requestPort)
{
  std::string prefix{ "tcp://" };
  if (!a_address.compare(0, prefix.size(), prefix)) {
    m_pimpl->ip = a_address;
    m_pimpl->ip.erase(0, prefix.size());
  }
  m_pimpl->publish_port = a_publishPort;
  m_pimpl->request_port = a_requestPort;

  char publishAddres[256]{};
  snprintf(publishAddres, 256, "%s:%d", a_address.c_str(), a_publishPort);
  char incomingAddress[256]{};
  snprintf(incomingAddress, 256, "%s:%d", a_address.c_str(), a_requestPort);
  m_pimpl->publish_socket->bind(publishAddres);
  m_pimpl->incoming_socket->bind(incomingAddress);
}

void Server::connect(std::string const &a_broker)
{
  m_pimpl->broker_socket->connect(a_broker);
}

void Server::registerTopics(topics_t const &a_topics)
{
  registerTopics(a_topics, true);
}

void Server::unregisterTopics()
{
  if (!m_pimpl->registered) return;

  m_pimpl->topics.clear();
  m_pimpl->registered = false;

  static message_t msg{};

  SERIALIZE_HASH("UnpublishRequest", msg);
  send(Socket::eBroker, msg, ZMQ_SNDMORE);

  UnpublishRequest unpublishRequest{};
  get_client_info(unpublishRequest.mutable_client());
  unpublishRequest.mutable_client()->set_hostname(broker::gethostbyaddr(m_pimpl->ip));
  unpublishRequest.mutable_client()->set_publish_port(m_pimpl->publish_port);
  unpublishRequest.mutable_client()->set_request_port(m_pimpl->request_port);

  unpublishRequest.SerializeToString(&msg);
  send(Socket::eBroker, msg);

  receive(Socket::eBroker);
}

void Server::ping()
{
  if (!m_pimpl->registered) return;

  using namespace std::chrono_literals;
  using std::chrono::duration;
  using std::chrono::duration_cast;
  using clock = std::chrono::high_resolution_clock;

  static auto last{ clock::now() };

  auto now{ clock::now() };
  auto diff_ms{ duration_cast<std::chrono::milliseconds>(now - last) };

  static message_t msg{};

  if (diff_ms.count() >= 200) {
    last = now;

    SERIALIZE_HASH("PingRequest", msg);
    send(Socket::eBroker, msg, ZMQ_SNDMORE);

    broker::PingRequest ping{};
    get_client_info(ping.mutable_client());
    ping.mutable_client()->set_hostname(broker::gethostbyaddr(m_pimpl->ip));
    ping.mutable_client()->set_publish_port(m_pimpl->publish_port);
    ping.mutable_client()->set_request_port(m_pimpl->request_port);

    ping.SerializeToString(&msg);
    send(Socket::eBroker, msg);

    messages_t reply{ receive(Socket::eBroker) };
    broker::PongResponse response{};
    response.ParseFromString(reply[0]);

    if (response.result() == broker::PongResponse::RECONNECT) {
      m_pimpl->registered = false;
      std::cerr << "Error: DISCONNECTED, reregistering..\n";
      std::this_thread::sleep_for(100ms);
      registerTopics(m_pimpl->topics, false);
    }
  }
}

bool Server::hasIncoming() const
{
  zmq::poll(&m_pimpl->incoming_poll, 1, 0);
  return (m_pimpl->incoming_poll.revents & ZMQ_POLLIN) == ZMQ_POLLIN;
}

void Server::close()
{
  unregisterTopics();
  m_pimpl->broker_socket->close();
  m_pimpl->publish_socket->close();
  m_pimpl->incoming_socket->close();
}

zmq::socket_t *Server::getSocket(Socket const &a_socket) const
{
  switch (a_socket) {
    case Socket::eBroker: return m_pimpl->broker_socket.get();
    case Socket::ePublish: return m_pimpl->publish_socket.get();
    case Socket::eIncoming: return m_pimpl->incoming_socket.get();
  }

  return nullptr;
}

void Server::send(Socket const &a_socket, message_t const &a_message, int32_t const &a_flags)
{
  zmq::socket_t *const socket{ getSocket(a_socket) };
  assert(socket);

  broker::send(socket, a_message, a_flags);
}

void Server::send(Socket const &a_socket, messages_t const &a_messages, int32_t const &a_flags)
{
  if (a_messages.empty()) return;

  zmq::socket_t *const socket{ getSocket(a_socket) };
  assert(socket);

  broker::send(socket, a_messages, a_flags);
}

messages_t Server::receive(Socket const &a_socket, int32_t const &a_flags) const
{
  zmq::socket_t *const socket{ getSocket(a_socket) };
  assert(socket);

  return broker::receive(socket, a_flags);
}

void Server::registerTopics(topics_t const &a_topics, bool a_store)
{
  if (a_topics.empty()) return;

  message_t msg{};

  PublishRequest publishRequest{};
  get_client_info(publishRequest.mutable_client());
  publishRequest.mutable_client()->set_hostname(broker::gethostbyaddr(m_pimpl->ip));
  publishRequest.mutable_client()->set_publish_port(m_pimpl->publish_port);
  publishRequest.mutable_client()->set_request_port(m_pimpl->request_port);

  for (auto const &topic : a_topics) publishRequest.add_topics(topic);

  SERIALIZE_HASH("PublishRequest", msg);
  send(Socket::eBroker, msg, ZMQ_SNDMORE);

  publishRequest.SerializeToString(&msg);
  send(Socket::eBroker, msg);

  messages_t messages{ receive(Socket::eBroker) };
  assert(messages.size() == 1);

  PublishResponse response{};
  response.ParseFromString(messages[0]);
  int32_t const responseResult{ response.result() };
  switch (responseResult) {
    case PublishResponse::OK:
      m_pimpl->registered = true;
      break;
    case PublishResponse::NOT_INITIALIZED:
      break;
    case PublishResponse::EMPTY:
      break;
    case PublishResponse::PARTIAL:
      m_pimpl->registered = true;
      break;
    default: break;
  }

  if (!a_store) return;

  m_pimpl->topics.reserve(m_pimpl->topics.size() + a_topics.size());
  std::move(std::begin(a_topics), std::end(a_topics), std::back_inserter(m_pimpl->topics));
  std::sort(std::begin(m_pimpl->topics), std::end(m_pimpl->topics));
  m_pimpl->topics.erase(std::unique(std::begin(m_pimpl->topics), std::end(m_pimpl->topics)), std::end(m_pimpl->topics));
}

} // namespace broker
