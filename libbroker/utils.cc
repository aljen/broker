// MIT License
//
// Copyright (c) 2017-2018 Artur Wyszyński, aljen at hitomi dot pl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "broker/utils.h"
#include "broker/server.h"

#include <signal.h>
#include <map>
#include <memory>
#include <zmq-cpp/zmq.hpp>

#if BROKER_PLATFORM_WINDOWS
#include <shlwapi.h>
#include <process.h>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <netdb.h>
#include <unistd.h>
#endif

#include "broker/broker_common.pb.h"

namespace broker {

std::shared_ptr<zmq::context_t> g_context{ std::make_shared<zmq::context_t>() };

std::string gethostname()
{
#if BROKER_PLATFORM_WINDOWS && !defined(HOST_NAME_MAX)
#define HOST_NAME_MAX 1024
#endif
  char temp[HOST_NAME_MAX];

#if BROKER_PLATFORM_WINDOWS
  int32_t const ret{ ::gethostname(temp, HOST_NAME_MAX) };
  if (ret == -1) {
    WSADATA wsaData{};
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    ::gethostname(temp, HOST_NAME_MAX);
    WSACleanup();
  }
#else
  ::gethostname(temp, HOST_NAME_MAX);
#endif

  std::string hostname{ temp };
  return hostname;
}

std::string gethostbyaddr(std::string const &ip)
{
  std::string host{};
  addrinfo *result{};

  addrinfo hints{};
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  int32_t error{ getaddrinfo(ip.c_str(), nullptr, &hints, &result) };

  if (error != 0) return host;
  for (auto res = result; res != nullptr; res = res->ai_next) {
    char hostname[NI_MAXHOST]{};
    error = getnameinfo(res->ai_addr, res->ai_addrlen, hostname, NI_MAXHOST, nullptr, 0, NI_NOFQDN);
    if (error != 0) continue;
    if (*hostname != '\0') {
      host = hostname;
      break;
    }
  }

  freeaddrinfo(result);

  return host;
}

void catch_em_all(const signals_t &a_quit, const signals_t &a_debug, const signals_t &a_ignore)
{
  static const std::map<int32_t, char const *> sigToString{ { SIGINT, "SIGINT" },   { SIGILL, "SIGILL" },
                                                            { SIGFPE, "SIGFPE" },   { SIGSEGV, "SIGSEGV" },
                                                            { SIGTERM, "SIGTERM" }, { SIGABRT, "SIGABRT" } };

  auto handler = [](int32_t sig) -> void {
    fprintf(stderr, "Got signal = %d (%s)\n", sig,
            (sigToString.find(sig) != sigToString.end() ? sigToString.find(sig)->second : "UNKNOWN"));
    broker::Server &server{ broker::server() };
    server.unregisterTopics();
    server.close();
    exit(sig);
  };

  auto debug_handler = [](int32_t sig) -> void {
    fprintf(stderr, "Got signal = %d (%s), waiting for debugger..\n", sig,
            (sigToString.find(sig) != sigToString.end() ? sigToString.find(sig)->second : "UNKNOWN"));
    broker::Server &server{ broker::server() };
    server.unregisterTopics();
    server.close();
#if BROKER_COMPILER_MSVC
    __debugbreak();
#else
    __builtin_trap();
#endif
  };

  for (auto const &sig : a_quit) signal(sig, handler);
  for (auto const &sig : a_debug) signal(sig, debug_handler);
  for (auto const &sig : a_ignore) signal(sig, SIG_IGN);
}

void get_client_info(ClientInfo *const a_info)
{
  static ClientInfo info{};

  bool const initialized{ info.IsInitialized() };

  if (initialized) {
    a_info->CopyFrom(info);
    return;
  }

#ifndef MAX_PATH
#define MAX_PATH 4098
#endif
#if BROKER_PLATFORM_WINDOWS && !defined(getpid)
#define getpid _getpid
#endif

  char name[MAX_PATH]{};
#if BROKER_PLATFORM_WINDOWS
  GetModuleFileNameA(0, name, MAX_PATH);
#elif BROKER_PLATFORM_LINUX
  readlink("/proc/self/exe", name, MAX_PATH);
#else
#error Unknown platform
#endif

  info.set_name(name);
  info.set_pid(getpid());

  a_info->CopyFrom(info);
}

void send(zmq::socket_t *const a_socket, message_t const &a_message, int32_t const &a_flag)
{
  zmq::message_t message(a_message.size());
  memcpy(message.data(), a_message.data(), a_message.size());
  a_socket->send(message, a_flag);
}

void send(zmq::socket_t *const a_socket, messages_t const &a_messages, int32_t const &a_flag)
{
  if (a_messages.empty()) return;

  size_t const size{ a_messages.size() };
  for (size_t i = 0; i < size; ++i) {
    bool const last{ i == size - 1 };
    zmq::message_t message(a_messages[i].size());
    memcpy(message.data(), a_messages[i].data(), a_messages[i].size());
    a_socket->send(message, last ? a_flag : (a_flag | ZMQ_SNDMORE));
  }
}

messages_t receive(zmq::socket_t *const a_socket, int32_t const &a_flags)
{
  messages_t messages{};
  bool done{};
  while (!done) {
    zmq::message_t message{};
    a_socket->recv(&message, a_flags);
    message_t msg(static_cast<char *>(message.data()), message.size());
    messages.push_back(msg);
    done = !message.more();
  }
  return messages;
}

} // namespace broker
