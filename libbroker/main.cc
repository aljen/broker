#include <QCoreApplication>
#include <QDebug>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <signal.h>
#ifndef WIN32
# include <unistd.h>
#else
# include <windows.h>
# define sleep(x) Sleep(x * 1000)
#endif
#include "brokerclient.h"

using Signals = std::vector<int>;

static void
catchEmAll(Signals const& quit, Signals const& debug = Signals(), Signals const& ignore = Signals())
{
  static const std::map<int, char const *> sigToString{
      {SIGINT, "SIGINT"},   {SIGILL, "SIGILL"},     {SIGFPE, "SIGFPE"},  {SIGSEGV, "SIGSEGV"},
      {SIGTERM, "SIGTERM"}, {SIGABRT, "SIGABRT"}};

  auto handler = [](int sig) -> void {
    fprintf(stderr, "Got signal = %d (%s)\n", sig,
            (sigToString.find(sig) != sigToString.end() ? sigToString.find(sig)->second : "UNKNOWN"));
    QCoreApplication::quit();
  };

  auto debug_handler = [](int sig) -> void {
    fprintf(stderr, "Got signal = %d (%s), waiting for debugger..\n", sig,
            (sigToString.find(sig) != sigToString.end() ? sigToString.find(sig)->second : "UNKNOWN"));
#ifdef _MSC_VER
    __debugbreak();
#else
    __builtin_trap();
#endif
  };

  for (auto sig : quit)   signal(sig, handler);
  for (auto sig : debug)  signal(sig, debug_handler);
  for (auto sig : ignore) signal(sig, SIG_IGN);
}

int
main(int argc, char **argv)
{
  (void)argc;
  (void)argv;

  catchEmAll({SIGINT}, {SIGILL, SIGFPE, SIGSEGV, SIGTERM, SIGABRT});

#if 0
  if (argc <= 1) {
    return 0;
  }

  QCoreApplication app(argc, argv);

  Broker client(&app);

  for (int i = 1; i < argc; ++i)
    client.add_topic(argv[i]);

  client.send_message();

  return app.exec();
#else
  broker::Publisher &publisher = broker::publisher();
  publisher.connectTo("tcp://127.0.0.1:1234");
  publisher.addTopic("DUPA1");
  publisher.addTopic("DUPA2");
  publisher.addTopic("DUPA3");
  for (int i = 1; i < argc; ++i)
    publisher.addTopic(argv[i]);
  publisher.publish();
  sleep(3);
  publisher.unpublish();
  return 0;
#endif
}
